
let vtv_1 = ['Video/vtv1_1.mp4',
    'Video/vtv1_2.mp4'];
let vtv_2 = ['Video/vtv2_1.mp4',
    'Video/vtv2_2.mp4'];
let vtv_3 = ['Video/vtv3_1.mp4',
'Video/vtv3_2.mp4'];
let vtv_6 = ['Video/vtv6_1.mp4'];
let vtv_7 = ['Video/vtv7_1.mp4',
    'Video/vtv7_2.mp4',
    'Video/vtv7_3.mp4'];
let the_thao = ['Video/vtv3.mp4',
    'Video/bongda.mp4'];
let gioi_tre = ['Video/quangcao.mp4',
    'Video/canhac.mp4'];
let thoi_trang = ['Video/ftv1.mp4',
    'Video/ftv2.mp4'];
let phim_truyen = ['Video/quangcao.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4'];
let the_gioi = ['Video/discovery1.mp4'];
let ha_noi1 = ['Video/hanoi1_1.mp4',
    'Video/hanoi1_2.mp4'];
let ha_noi2 = ['Video/quangcao.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4',
    'Video/vtv3.mp4',];
let q_tri = ['Video/quangtri1.mp4',
    'Video/quangtri2.mp4'];
let tt_hue = ['Video/hue1.mp4',
    'Video/hue2.mp4'];
let da_nang = ['Video/danang1.mp4',
    'Video/danang2.mp4'];
let gia_lai = ['Video/quangcao.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4',
    'Video/vtv3.mp4'];
let v_long1 = ['Video/vinhlong1_1.mp4',
    'Video/vinhlong1_2.mp4'];
let v_long2 = ['Video/quangcao.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4',
    'Video/vtv3.mp4'];
let hcm7 = ['Video/htv7_1.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4'];
let hcm9 = ['Video/quangcao.mp4',
    'Video/canhac.mp4',
    'Video/bongda.mp4'];
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
function open_kenh(ten_kenh) {
    let duong_dan_video = '';
    if (ten_kenh == 'vtv_1') {

        duong_dan_video = vtv_1[getRandomInt(3)];
    }
    if (ten_kenh == 'vtv_2') {
        duong_dan_video = vtv_2[getRandomInt(3)];
    }
    if (ten_kenh == 'vtv_3') {
        duong_dan_video = vtv_3[getRandomInt(3)];
    }
    if (ten_kenh == 'vtv_6') {
        duong_dan_video = vtv_6[getRandomInt(3)];
    }
    if (ten_kenh == 'the_thao') {

        duong_dan_video = the_thao[getRandomInt(3)];
    }
    if (ten_kenh == 'gioi_tre') {

        duong_dan_video = gioi_tre[getRandomInt(3)];
    }
    if (ten_kenh == 'vtv_7') {

        duong_dan_video = vtv_7[getRandomInt(3)];
    }
    if (ten_kenh == 'thoi_trang') {

        duong_dan_video = thoi_trang[getRandomInt(3)];
    }
    if (ten_kenh == 'phim_truyen') {

        duong_dan_video = phim_truyen[getRandomInt(3)];
    }
    if (ten_kenh == 'the_gioi') {

        duong_dan_video = the_gioi[getRandomInt(3)];
    }
    if (ten_kenh == 'ha_noi1') {

        duong_dan_video = ha_noi1[getRandomInt(3)];
    }
    if (ten_kenh == 'ha_noi2') {

        duong_dan_video = ha_noi2[getRandomInt(3)];
    }
    if (ten_kenh == 'q_tri') {

        duong_dan_video = q_tri[getRandomInt(3)];
    }
    if (ten_kenh == 'tt_hue') {

        duong_dan_video = tt_hue[getRandomInt(3)];
    }
    if (ten_kenh == 'da_nang') {

        duong_dan_video = da_nang[getRandomInt(3)];
    }
    if (ten_kenh == 'gia_lai') {

        duong_dan_video = gia_lai[getRandomInt(3)];
    }
    if (ten_kenh == 'v_long1') {

        duong_dan_video = v_long1[getRandomInt(3)];
    }
    if (ten_kenh == 'v_long2') {

        duong_dan_video = v_long2[getRandomInt(3)];
    }
    if (ten_kenh == 'hcm7') {

        duong_dan_video = hcm7[getRandomInt(3)];
    }
    if (ten_kenh == 'hcm9') {

        duong_dan_video = hcm9[getRandomInt(3)];
    }
    open_video(duong_dan_video);

}

function open_video(duong_dan_video) {
    document.getElementById('div_video').innerHTML = '<video autoplay controls id="video_ctrl" width="850" height="auto"><source src="' + duong_dan_video + '" type="video/ogg"></video>';
    document.getElementById('video_ctrl').play();
}
